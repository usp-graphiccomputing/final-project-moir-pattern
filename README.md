# Final Project - Moiré Pattern from Scanned Photos

Image Processing - SCC5830. 

Final Project - Moiré Pattern from Scanned Photos

Authors:
* Alexis J. Vargas (Usp Number: 11939710)
* Karelia A. Vilca (Usp Number: 11939727)

Semester 1, Year: 2020
## Abstract
This project aims to remove wavy patterns known as Moiré in images and see how several methods can be applied to achieve this goal. 

Our main image processing task is image restoration and the method will be applied in different images that present the Moiré effect.

<!-- 
<img src="images/abstract0.jpeg" width="300"> 

<img src="images/abstract1.jpg" width="300">

Moiré pattern created by superimposing two grids. 

<img src="images/abstract2.png" width="300">
-->

## Main Objetive
Compare different techniques to eliminate the moiré pattern and try to maintain image quality.

**Specific objectives (desired approaches):**
- Test with real images, for example video frames or photos to screens. Even with manually altered images the techniques perform better, they do not represent a real scene.
- Compare the Fourier spectrum to see the impact of the technique.
- In the case of video, reassemble the frames to improve the quality of the entire clip.

## Input Images 

We use different types of test images to develop different experiments.

### A. Focused frame

From the video "Fuji X30 video sample Moire" taken from Youtube [3] we can see that it presents Moiré patterns in certain frames due to the movement of the camera, this is more noticeable in regions like the roof. So we cut through a problem region to test the technique. 
With open CV we load the video, capture a specific frame and delimit the region.

<img src="notebooks/frames/frame44_roof.jpg" width="300"> 

### B. Full Frame

One of the expected objectives is to be able to reconstruct a video clip, so we take the full frame with affected regions and others do not and apply the techniques. The problem is that the level of detail falls throughout the image.

<img src="notebooks/frames/frame44.jpg" width="300"> 

### C. Photo to screen

This image obtained from [4] presents common noise on screens.

<img src="notebooks/frames/screen.jpeg" width="300"> 

### D. Emulated noise

The noise can be emulated with moiré, vertical, horizontal and diagonal patterns. We take a smooth image of [2] and apply the different patterns and techniques.

<img src="notebooks/frames/house4.png" width="300"> 



## Pipeline 

1. We receive an initial image, any of the proposed experiments.

2. We apply the different filtering methods for treating the Moiré problem.

    2.1. Median filters:
        - Divide into RGB channels
        - Go through the neighboring pixels and get a median. This for each channel.
        - Join the channels
  
    2.2.  Bilateral filtering
        - Filter the image using gaussian kernels of size 18, and a filter sigma of 50 for color and coordinate spaces.}
  
    2.3.  Low pass filter
        - Create a low pass filter
        - Filter the image using low pass filter in Fourier domain
  
    2.4.  Bandstop
        - Apply Low-pass filter for image degradation 
        - Apply threshold filter for higher frequencies
  
    2.5.  Cut
        - Obtain the Fourier spectrum
        - Cut the center cross
        - Apply the filter
    
3. We show the original image with its respective Fourier spectrum. The same with each of the output images of each technique


## Initial code

Several methods need to decompose the image into channels, so we give this general function. And the Fourier spectrum function to compare.
* [Initial code](/notebooks/Partial-Report.ipynb) contains first techniques, experiments and results.

## Bibliography

[1] Hazavei, S. M., & Shahdoosti, H. R. (2017). A New Method for Removing the Moire'Pattern from Images. arXiv preprint arXiv:1701.09037.

[2] gmelodie/silentMoire. (2019). Retrieved 15 June 2020, from https://github.com/gmelodie/silentMoire

[3] Camerahoarders video samples. (2015). Retrieved 15 June 2020, from https://www.youtube.com/watch?v=273iyerqyE4

[4] Shutterstock. 2020. Bad Tv Signal On The​: Vídeo Stock (100% Livre De Direitos) 32432830 | Shutterstock. [online] Available at: <https://www.shutterstock.com/pt/video/clip-32432830-bad-tv-signal-on-screen-lines-background> [Accessed 15 June 2020].

## Folders and files:
* [Images](/images) contains images used in the demos
* [Notebooks](/notebooks) contains notebooks developed from implementations.
* [Partial Report](/notebooks/Partial-Report.ipynb) contains Items corresponding to the partial report.